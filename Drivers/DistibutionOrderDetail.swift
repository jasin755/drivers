//
//  DistibutionOrder.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Argo
import Curry


struct DistributionOrderDetail {
    let id : Int
    let paymentId : Int
    let paymentName : String
    let currencyId : Int
    let currencyName : String
    let boxes : [Box]
    let expectedPrice : Float
    let totalPrice : Float
}

struct Box {
    let id : Int
    let ean : String
}

extension DistributionOrderDetail : Decodable{
    
    static func decode(json: JSON) -> Decoded<DistributionOrderDetail> {
        
        var j = json
        
        if let distributionOrder : JSON = (j <| "distribution_order").value{
            j = distributionOrder
        }
        
        
        return curry(DistributionOrderDetail.init)
            <^> j <| "id_distribution_order"
            <*> j <| "id_payment"
            <*> j <| "payment"
            <*> j <| "id_currency"
            <*> j <| "currency"
            <*> j <|| "boxes"
            <*> j <| "expected_price"
            <*> j <| "total_price"
    }

}


extension Box : Decodable{

    static func decode(j: JSON) -> Decoded<Box> {
        return curry(Box.init)
            <^> j <| "box_id"
            <*> j <| "ean"
    }
    
    
}