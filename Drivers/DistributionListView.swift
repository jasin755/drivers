//
//  DistributionList.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa

class DistributionListView : UITableView, UITableViewDataSource, UITableViewDelegate{

    private let cellId = "distributionListCellId"
    var selectedDistribution : ((distribution : Distribution) -> ())?
    
    
    let distributions = MutableProperty<Distributions?>(nil)
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        dataSource = self
        delegate = self
        registerClass(DistributionCellView.self, forCellReuseIdentifier: cellId)
        
        distributions
            .producer
            .startWithNext{[weak self] distributions in
                self!.reloadData()
            }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count = 0
        
        if let dist = distributions.value{
            count = dist.count
        }
        
        return count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! DistributionCellView
        
        let dists = distributions.value!
        cell.distribution = dists[indexPath.row]
    
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let s = selectedDistribution{
            s(distribution: distributions.value![indexPath.row])
        }
    }
    
}