//
//  DistributionOrderDetailView.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class DistributionOrderDetailView : BaseView{

    
    private let headerLabel = UILabel()
    private let distributionOrderSummaryView = DistributionOrderSummaryView()
    
    var distributionOrderDetail : DistributionOrderDetail? {
        didSet{
            if let detail = distributionOrderDetail{
                distributionOrderSummaryView.totalPriceValue.text = "\(detail.totalPrice) \(detail.currencyName)"
                distributionOrderSummaryView.paymentTypeValue.text = detail.paymentName
                distributionOrderSummaryView.expectedPriceValue.text = "\(detail.expectedPrice) \(detail.currencyName)"
            }
        }
    }
    
    
    var distributionOrder : DistributionOrder? {
        didSet{
            if let order = distributionOrder {
                headerLabel.text = "Rozvoz \(order.arivalDate)"
                distributionOrderSummaryView.header2Label.text = "\(order.customerFullname) \(order.orderNumber)"
            }
        }
    }
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        headerLabel.font = UIFont(name: "NimbusSanL-Bol", size: 20)
        headerLabel.textColor = .whiteColor()
        
        let border = UIView()
        border.backgroundColor = .whiteColor()

        addSubview(headerLabel)
        addSubview(border)
        addSubview(distributionOrderSummaryView)
        
        headerLabel.snp_makeConstraints{[weak self] make in
            make.top.equalTo(self!).offset(40)
            make.leading.equalTo(self!).offset(10)
        }
        
        border.snp_makeConstraints{[weak self] make in
            make.top.equalTo(self!.headerLabel.snp_bottom)
            make.height.equalTo(1)
            make.width.equalTo(self!)
        }
   
        distributionOrderSummaryView.snp_makeConstraints{[weak self] make in
            make.top.equalTo(border.snp_bottom).offset(15)
            make.left.equalTo(self!)
            make.right.equalTo(self!)
            make.bottom.equalTo(self!.distributionOrderSummaryView.expectedPriceLabel.snp_bottom).offset(15)
        }
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}