//
//  DistributionCellView.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome_swift

class DistributionCellView : UITableViewCell{

    var distribution : Distribution?{
        didSet{
            if let d = distribution{
                dateStart.text = d.dateStart
                dateEnd.text = d.dateEnd
            }else{
                dateStart.text = ""
                dateEnd.text = ""
            }
        }
    }

    private let dateStart = UILabel()
    private let dateEnd = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
        let dash = UILabel()
        dash.text = "-"
        
        let arrow = UILabel()
        arrow.font = UIFont.fontAwesomeOfSize(16)
        arrow.text = String.fontAwesomeIconWithName(FontAwesome.ArrowRight)
        arrow.textColor = .blackColor()
        
        addSubview(dateStart)
        addSubview(dateEnd)
        addSubview(dash)
        addSubview(arrow)
    
        
        dateStart.snp_makeConstraints{make in
            make.left.equalTo(self).offset(10)
            make.centerY.equalTo(self)
        }
        
        dash.snp_makeConstraints{make in
            make.left.equalTo(dateStart.snp_right).offset(5)
            make.centerY.equalTo(dateStart)
        }
        
        dateEnd.snp_makeConstraints{make in
            make.left.equalTo(dash.snp_right).offset(5)
            make.centerY.equalTo(dateStart)
        }
        
        arrow.snp_makeConstraints{make in
            make.right.equalTo(self).offset(-20)
            make.centerY.equalTo(self)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}