//
//  BaseView.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 18.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class BaseView : UIView{

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(red: 55/255, green: 148/255, blue: 213/255, alpha: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}