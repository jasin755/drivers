//
//  UserNameView.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 16.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome_swift

    class UserNameView : UIView{

        var userName : String?{
            didSet{
                userNameLabel.text = userName
            }
        }
        
        private let userNameLabel = UIView.createLabel()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            let userIcon = UILabel()
            userIcon.font = UIFont.fontAwesomeOfSize(16)
            userIcon.text = String.fontAwesomeIconWithName(FontAwesome.User)
            userIcon.textColor = .whiteColor()
            
            userNameLabel.textColor = .whiteColor()
            
            addSubview(userNameLabel)
            addSubview(userIcon)
            
            userNameLabel.snp_makeConstraints{[weak self] make in
                make.centerY.equalTo(self!)
                make.trailing.equalTo(self!)
            }
            
            userIcon.snp_makeConstraints{make in
                make.right.equalTo(userNameLabel.snp_left).offset(-8)
                make.centerY.equalTo(userNameLabel)
            }
            
        }
        
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

    }