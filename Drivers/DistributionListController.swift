//
//  DistributionController.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 16.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa

class DistributionListController : BaseViewController{

    private var _distributionViewModel : DistributionViewModel
    private var _distributionOrderSplitViewController : DistributionOrderSplitViewController
    
    init(distributionViewModel : DistributionViewModel, distributionOrderSplitViewController : DistributionOrderSplitViewController) {
        _distributionViewModel = distributionViewModel
        _distributionOrderSplitViewController = distributionOrderSplitViewController
        super.init(nibName: nil, bundle: nil)
        navigationItem.setHidesBackButton(true, animated: false)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let distributionListView = DistributionListView()
        distributionListView.selectedDistribution = {[weak self] distribution in
            self!._distributionViewModel.selectDistribution(distribution)
            self!.presentViewController(self!._distributionOrderSplitViewController, animated: true, completion: nil)
        }
        
        view.addSubview(distributionListView)
        
        distributionListView.snp_makeConstraints{make in
            make.edges.equalTo(view)
        }
        
        
        distributionListView.distributions <~ _distributionViewModel.distributions
        

    }
    
}