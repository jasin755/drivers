//
//  ViewController.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 14.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import UIKit
import ReactiveCocoa
import SnapKit
import enum Result.NoError

class LoginController: BaseViewController {

    private var _driverViewModel : DriverViewModel
    
    init(driverViewModel: DriverViewModel, distributionListNavigationController : DistributionListNavigationController) {
        _driverViewModel = driverViewModel
        
        super.init(nibName: nil, bundle: nil)
        
        
        driverViewModel
            .driver
            .signal
            .observeNext{[weak self] driver in
                if driver != nil{
                   self!.presentViewController(distributionListNavigationController, animated: true, completion: nil)
                }
                
            }        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let loginView = LoginView()
        view = loginView
        
        let loginButton = loginView.loginForm.button
        let loginName = loginView.loginForm.loginInput
        let loginPassword = loginView.loginForm.passwordInput
        
        
        RACSignal
            .combineLatest([loginName.rac_textSignal(), loginPassword.rac_textSignal()])
            .subscribeNext{tuple in
                let racTuple = tuple as! RACTuple
                loginButton.enabled = ((racTuple.first) as? String)?.characters.count > 0 && ((racTuple.second) as? String)?.characters.count > 0
                
                if loginButton.enabled{
                    loginButton.backgroundColor = .whiteColor()
                }else{
                    loginButton.backgroundColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1)
                }
                
            }
        
            loginButton
                .rac_signalForControlEvents(UIControlEvents.TouchUpInside)
                .toSignalProducer()
                .startWithNext{[weak self] _ in
                    self!._driverViewModel.login(loginName.text!, password: loginPassword.text!)
                }

        
    }
}

