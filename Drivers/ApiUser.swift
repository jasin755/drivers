//
//  ApiUser.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 14.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Argo
import Curry

struct ApiUser{
    let token : String
}

extension ApiUser : Decodable{
    static func decode(j: JSON) -> Decoded<ApiUser> {
        return curry(ApiUser.init)
        <^> j <| "ExsysAuthenticateToken"
    }
}

