//
//  DriverViewModel.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 19.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import ReactiveCocoa

class DriverViewModel{

    var _distributionProvider : DistributionProvider
    
    let driver = MutableProperty<Driver?>(nil)
    
    init(distributionProvider : DistributionProvider){
        _distributionProvider = distributionProvider
        
        driver <~ distributionProvider.driver
        
    }
    
    func login(login : String, password : String){
        _distributionProvider.login(login, password: password)
    }
    
}