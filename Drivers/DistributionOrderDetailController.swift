//
//  DistributionOrderDetailController.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa
import enum Result.NoError

class DistributionOrderDetailController : UIViewController{
    
    private let distributionOrderDetail = MutableProperty<DistributionOrderDetail?>(nil)
    private var distributionOrderDetailView : DistributionOrderDetailView?
    
    init(distributionViewModel : DistributionViewModel) {
        super.init(nibName: nil, bundle: nil)

        distributionViewModel
            .distributionOrderDetail
            .producer
            .combineLatestWith(distributionViewModel.distributionOrder.producer)
            .startWithNext{tuple in
                
                if let detail = tuple.0, let order = tuple.1{
                    if (self.distributionOrderDetailView == nil){
                        self.initDistributionOrderDetailView()
                    }

                    self.distributionOrderDetailView?.distributionOrderDetail = detail
                    self.distributionOrderDetailView?.distributionOrder = order
                    
                }else{
                    if let v = self.distributionOrderDetailView{
                        v.removeFromSuperview()
                    }
                }
            }
    
        view = BaseView()
    }
    
    private func initDistributionOrderDetailView(){
        
        distributionOrderDetailView = DistributionOrderDetailView()
        view.addSubview(distributionOrderDetailView!)
        distributionOrderDetailView!.snp_makeConstraints{make in
            make.edges.equalTo(view)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}