//
//  DistributionOrders.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Argo
import Curry

struct DistributionOrders : CollectionType{
    
    private let distributionOrders : [DistributionOrder]
    
    init(distributionOrders : [DistributionOrder]){
        self.distributionOrders = distributionOrders
    }
    
    var startIndex: Int {
        return 0
    }
    
    var endIndex: Int {
        return distributionOrders.count
    }
    
    subscript(i: Int) -> DistributionOrder {
        return distributionOrders[i]
    }
    
}

struct DistributionOrder{
    let id : Int
    let customerFullname : String
    let arivalDate : String
    let arivalTime : String
    let orderNumber : String

}

extension DistributionOrders : Decodable{
    
    static func decode(j: JSON) -> Decoded<DistributionOrders> {
        
        return curry(DistributionOrders.init)
            <^> j <|| "distribution_orders"
    }
    
}

extension DistributionOrder : Decodable{
    
    static func decode(j: JSON) -> Decoded<DistributionOrder> {
        
        return curry(DistributionOrder.init)
            <^> j <| "id_distribution_order"
            <*> j <| "customer_fullname"
            <*> j <| "arrival_date"
            <*> j <| "arrival_time"
            <*> j <| "order_number"
    }
    
}