//
//  DistributionOrderSplitView.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class DistributionOrderSplitViewController : UISplitViewController{

    init(distributionViewModel : DistributionViewModel, distributionOrderNavigationController : DistributionOrderNavigationController) {
        super.init(nibName: nil, bundle: nil)
        
        viewControllers = [distributionOrderNavigationController]
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}