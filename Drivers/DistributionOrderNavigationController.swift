//
//  DistributionOrderNavigationController.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class DistributionOrderNavigationController : BaseNavigationController{

    init(distributionViewModel : DistributionViewModel, distributionOrderListController : DistributionOrderListController) {
        super.init()
        viewControllers = [distributionOrderListController]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}