//
//  LoginForm.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 15.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class LoginForm : UIView{
    
    let loginInput = UIView.createTextField()
    let passwordInput = UIView.createTextField()
    let button = UIButton()

    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        addSubview(loginInput)
        addSubview(passwordInput)
        addSubview(button)
        
        
        loginInput.placeholder = "Jméno"
        loginInput.text = "ridic"
        
        loginInput.snp_makeConstraints {[weak self] make in
            make.top.equalTo(self!)
            make.leading.equalTo(self!).offset(40)
            make.trailing.equalTo(self!).inset(40)
            make.height.equalTo(40)
        }
        
        loginInput.backgroundColor = UIColor(red: 90/255, green: 177/255, blue: 225/255, alpha: 1)
        
        passwordInput.secureTextEntry = true
        passwordInput.placeholder = "*****"
        passwordInput.text = "honza"
        passwordInput.snp_makeConstraints{[weak self] make in
            make.top.equalTo(self!.loginInput.snp_bottom).offset(10)
            make.leading.equalTo(self!).offset(40)
            make.trailing.equalTo(self!).inset(40)
            make.height.equalTo(self!.loginInput)
        }
        
        passwordInput.backgroundColor = UIColor(red: 90/255, green: 177/255, blue: 225/255, alpha: 1)
        
        button.setTitle("Přihlásit se", forState: UIControlState.Normal)
        button.setTitleColor(UIColor(red: 55/255, green: 148/255, blue: 213/255, alpha: 1), forState: UIControlState.Normal)
        
        button.snp_makeConstraints {[weak self] make in
            make.top.equalTo(self!.passwordInput.snp_bottom).offset(10)
            make.leading.equalTo(self!).offset(40)
            make.trailing.equalTo(self!).inset(40)
            make.height.equalTo(self!.loginInput)
        }

    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}