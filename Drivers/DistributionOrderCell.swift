//
//  DistributionOrderCell.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class DistributionOrderCell : UITableViewCell{

    private let name = UIView.createLabel()
    private let dateTime = UIView.createLabel()
    private let orderNumber = UIView.createLabel()
    
    var distributionOrder : DistributionOrder?{
        didSet{
            if let d = distributionOrder{
                name.text = d.customerFullname
                dateTime.text = "\(d.arivalDate) \(d.arivalTime)"
                orderNumber.text = d.orderNumber
            }
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(name)
        contentView.addSubview(dateTime)
        contentView.addSubview(orderNumber)
    
        name.snp_makeConstraints{[weak self] make in
            make.left.equalTo(self!).offset(10)
            make.top.equalTo(self!).offset(10)
        }
        
        dateTime.snp_makeConstraints{make in
            make.left.equalTo(name.snp_right).offset(10)
            make.centerY.equalTo(name)
        }
        
        orderNumber.snp_makeConstraints{make in
            make.left.equalTo(name)
            make.top.equalTo(name.snp_bottom).offset(5)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}