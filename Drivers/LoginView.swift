//
//  LoginView.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 15.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class LoginView : BaseView{
    
    let loginForm = LoginForm()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let logoImageView = UIImageView(image: UIImage(named: "exiteria_logo"))
        
        addSubview(loginForm)
        addSubview(logoImageView)
        
        loginForm.snp_makeConstraints{make in
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(140)
            make.center.equalTo(self)
        }
        
        logoImageView.snp_makeConstraints{make in
            make.bottom.equalTo(loginForm.snp_top).offset(-20)
            make.centerX.equalTo(loginForm)
        }
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}