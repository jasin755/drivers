//
//  ResponseError.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 14.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import Argo
import Curry

struct ResponseError{
    let message : String
}

extension ResponseError : Decodable{
    static func decode(j: JSON) -> Decoded<ResponseError> {
        return curry(ResponseError.init)
        <^> j <| "error"
    }
}