//
//  DistributionOrderSummaryView.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 18.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class DistributionOrderSummaryView : UIView{

    let header2Label = UILabel()    
    let expectedPriceLabel = UIView.createLabel()
    
    let totalPriceValue = UIView.createLabel()
    let paymentTypeValue = UIView.createLabel()
    let expectedPriceValue = UIView.createLabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(red: 90/255, green: 177/255, blue: 225/255, alpha: 1)
        
        header2Label.font = UIFont(name: "NimbusSanL-Bol", size: 16)
        addSubview(header2Label)
        
        header2Label.snp_makeConstraints{[weak self] make in
            make.top.equalTo(self!).offset(15)
            make.leading.equalTo(self!).offset(15)
        }
        
        /////////
        
        let totalPriceLabel = UIView.createLabel()
        totalPriceLabel.text = "Hodnota zakázky:"
        addSubview(totalPriceLabel)
        
        totalPriceLabel.snp_makeConstraints{[weak self] make in
            make.leading.equalTo(self!).offset(15)
            make.top.equalTo(self!.header2Label.snp_bottom).offset(15)
        }
        
        addSubview(totalPriceValue)
        totalPriceValue.snp_makeConstraints{make in
            make.left.equalTo(totalPriceLabel.snp_right).offset(15)
            make.centerY.equalTo(totalPriceLabel)
        }
        
        /////////
        
        let paymentTypeLabel = UIView.createLabel()
        paymentTypeLabel.text = "Způsob úhrady:"
        addSubview(paymentTypeLabel)
        
        paymentTypeLabel.snp_makeConstraints{[weak self] make in
            make.leading.equalTo(self!).offset(15)
            make.top.equalTo(totalPriceLabel.snp_bottom).offset(5)
        }
        
        addSubview(paymentTypeValue)
        paymentTypeValue.snp_makeConstraints{make in
            make.left.equalTo(paymentTypeLabel.snp_right).offset(15)
            make.centerY.equalTo(paymentTypeLabel)
        }
        
        /////////
        
        expectedPriceLabel.text = "Očekávaná tržba:"
        addSubview(expectedPriceLabel)
        
        expectedPriceLabel.snp_makeConstraints{[weak self] make in
            make.leading.equalTo(self!).offset(15)
            make.top.equalTo(paymentTypeLabel.snp_bottom).offset(5)
        }
        
        addSubview(expectedPriceValue)
        expectedPriceValue.snp_makeConstraints{make in
            make.left.equalTo(expectedPriceLabel.snp_right).offset(15)
            make.centerY.equalTo(expectedPriceLabel)
        }
        

        /////////
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}