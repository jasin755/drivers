//
//  Driver.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 14.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import Argo
import Curry

struct Driver {
    let id : Int
    let name : String
}


extension Driver : Decodable{
    
    static func decode(j: JSON) -> Decoded<Driver> {
        
        var json = j
        
        if let driver : JSON = (j <| "driver").value{
            json = driver
        }
        
        return curry(Driver.init)
            <^> json <| "id"
            <*> json <| "name"
        
    }
}