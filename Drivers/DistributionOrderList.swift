//
//  DistributionOrderList.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa
import enum Result.NoError

class DistributionOrderList : UITableView, UITableViewDataSource, UITableViewDelegate{
    
    private let cellId = "distributionOrderCell"
    
    var selectedDistributionOrder : ((DistributionOrder) -> ())?
    
    var distributionOrders : DistributionOrders?{
        didSet{
            reloadData()
        }
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        dataSource = self
        delegate = self
        rowHeight = 50
        registerClass(DistributionOrderCell.self, forCellReuseIdentifier: cellId)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count = 0
        
        if let d = distributionOrders{
            count = d.count
        }
        
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId) as! DistributionOrderCell
        
        cell.distributionOrder = distributionOrders![indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let method = selectedDistributionOrder{
            method(distributionOrders![indexPath.row])
        }
    }
    
}