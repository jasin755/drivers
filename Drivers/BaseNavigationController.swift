//
//  BaseNavigationController.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit

class BaseNavigationController : UINavigationController{


    init(){
        super.init(nibName: nil, bundle: nil)
        navigationBar.barTintColor = UIColor(red: 90/255, green: 177/255, blue: 225/255, alpha: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}