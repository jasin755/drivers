//
//  Distributions.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

    import Foundation
    import Argo
    import Curry

struct Distributions : CollectionType{
    
    typealias Index = Int
    
    private let distributions : [Distribution]
    
    init(distributions : [Distribution]){
        self.distributions = distributions
    }
    
    var startIndex: Int {
        return 0
    }
    
    var endIndex: Int {
        return distributions.count
    }
    
    subscript(i: Int) -> Distribution {
        return distributions[i]
    }

}

struct Distribution{
    let id : Int
    let dateStart : String
    let dateEnd : String
}



    extension Distributions : Decodable{
        
        static func decode(j: JSON) -> Decoded<Distributions> {
        
            return curry(Distributions.init)
                <^> j <|| "distributions"
        }
        
    }

    extension Distribution : Decodable{
        static func decode(j: JSON) -> Decoded<Distribution> {
            return curry(Distribution.init)
                <^> j <| "id_distribution"
                <*> j <| "date_start"
                <*> j <| "date_end"
        }
    }
