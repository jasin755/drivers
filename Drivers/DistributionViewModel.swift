//
//  DistributionViewModel.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 19.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import ReactiveCocoa
import enum Result.NoError
import UIKit

class DistributionViewModel {

    let distributions = MutableProperty<Distributions?>(nil)
    let distributionOrders = MutableProperty<DistributionOrders?>(nil)
    let distributionOrder = MutableProperty<DistributionOrder?>(nil)
    let distributionOrderDetail = MutableProperty<DistributionOrderDetail?>(nil)
    
    private var _distributionProvider : DistributionProvider
    
    init(distributionProvider : DistributionProvider){
        _distributionProvider = distributionProvider
        distributions <~ distributionProvider
            .getDistributionList()
            .flatMap(.Latest){distributions in
                return SignalProducer<Distributions?, NoError>(value: distributions)
            }
    }
    
    func selectDistribution(distribution : Distribution){
        distributionOrders <~ _distributionProvider
            .getDistributionOrderList(distribution.id)
            .flatMap(.Latest){distributionOrders in
                return SignalProducer<DistributionOrders?, NoError>(value: distributionOrders)
            }
    }
    
    func selectDistributionOrder(distributionOrder : DistributionOrder){
        
        distributionOrderDetail <~ _distributionProvider
            .getDistributionOrderDetail(distributionOrder.id)
            .flatMap(.Latest){[weak self] distributionOrderDetail -> SignalProducer<DistributionOrderDetail?, NoError> in
                self!.distributionOrder <~ SignalProducer<DistributionOrder?, NoError>(value: distributionOrder)
                return SignalProducer<DistributionOrderDetail?, NoError>(value: distributionOrderDetail)
            }
    }
    
}