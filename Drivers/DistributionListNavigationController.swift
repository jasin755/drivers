//
//  MainNavigationController.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 16.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome_swift

class DistributionListNavigationController : BaseNavigationController{
    
    init(distributionListController : DistributionListController) {
        super.init()
        viewControllers = [distributionListController]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}