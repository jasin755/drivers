//
//  Alamofire.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 14.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveCocoa
import Argo

extension Manager{
    
    
    
    class func rac_jsonRequest<T : Decodable where T.DecodedType == T> (method: Alamofire.Method,_ URLString: URLStringConvertible, parameters: [String: AnyObject]? = nil, encoding: ParameterEncoding = .URL, headers: [String: String]? = nil) -> SignalProducer<T, NSError> {
        
        
        return SignalProducer<T, NSError> {sink, disposable in
                let request = Alamofire.request(method, URLString, parameters: parameters, encoding: encoding, headers: headers).responseJSON { response in
                    if let JSON = response.result.value{
                                 
                        if let decoded : T  = decode(JSON){
                            sink.sendNext(decoded)
                            sink.sendCompleted()
                        }
                    }
                }
            
            disposable.addDisposable({
                request.cancel()
            })
        }
        
    }
    
}