//
//  DistributionOrderListController.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 17.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa
import enum Result.NoError

class DistributionOrderListController : BaseViewController{

    private var _distributionViewModel : DistributionViewModel
    private var _distributionOrderDetailController : DistributionOrderDetailController
    
    init(distributionViewModel : DistributionViewModel, distributionOrderDetailController : DistributionOrderDetailController){
        _distributionViewModel = distributionViewModel
        _distributionOrderDetailController = distributionOrderDetailController
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let distributionOrderList = DistributionOrderList()
        
        let barButton = UIBarButtonItem()
        barButton.title = "< Zpět"
        barButton.target = self
        barButton.action = #selector(back)
        navigationItem.leftBarButtonItem = barButton
        
        view.addSubview(distributionOrderList)
        
        distributionOrderList.snp_makeConstraints{make in
            make.edges.equalTo(view)
        }
        
        
        _distributionViewModel
            .distributionOrders
            .producer
            .startWithNext{distributionOrders in
                distributionOrderList.distributionOrders = distributionOrders
        }
        
        distributionOrderList.selectedDistributionOrder = {[weak self] distributionOrder in
            self!._distributionViewModel.selectDistributionOrder(distributionOrder)
            self!.showDetailViewController(self!._distributionOrderDetailController, sender: nil)
        }
        
        
    }
    
    func back(){
//        presentViewController(DistributionListNavigationController(distributionProvider: distributionProvider), animated: true, completion: nil)
    }
    

    
}