//
//  AppDelegate.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 14.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        let container = Container()
        
        //Model
        container.register(AuthorizatorProvider.self){_ in AuthorizatorProvider()}
        container.register(DistributionProvider.self){r in DistributionProvider(authorizator: r.resolve(AuthorizatorProvider.self)!)}
        
        
        //ViewModel
        container.register(DriverViewModel.self){r in DriverViewModel(distributionProvider: r.resolve(DistributionProvider.self)!)}
        container.register(DistributionViewModel.self){r in DistributionViewModel(distributionProvider: r.resolve(DistributionProvider.self)!) }
        
        //View
        container.register(DistributionListNavigationController.self){r in DistributionListNavigationController(distributionListController: r.resolve(DistributionListController.self)!)}
        
        container.register(DistributionOrderDetailController.self){r in DistributionOrderDetailController(distributionViewModel: r.resolve(DistributionViewModel.self)!) }
        
        container.register(DistributionOrderListController.self){r in DistributionOrderListController(distributionViewModel: r.resolve(DistributionViewModel.self)!, distributionOrderDetailController: r.resolve(DistributionOrderDetailController.self)!)}
        
        container.register(DistributionOrderNavigationController.self){r in DistributionOrderNavigationController(distributionViewModel: r.resolve(DistributionViewModel.self)!, distributionOrderListController: r.resolve(DistributionOrderListController.self)!)}
        
        container.register(DistributionOrderSplitViewController.self){r in DistributionOrderSplitViewController(distributionViewModel: r.resolve(DistributionViewModel.self)!, distributionOrderNavigationController: r.resolve(DistributionOrderNavigationController.self)!) }
        
        container.register(DistributionListController.self){r in DistributionListController(distributionViewModel: r.resolve(DistributionViewModel.self)!, distributionOrderSplitViewController: r.resolve(DistributionOrderSplitViewController.self)!)}
        
        container.register(LoginController.self){r in LoginController(driverViewModel: r.resolve(DriverViewModel.self)!, distributionListNavigationController: r.resolve(DistributionListNavigationController.self)!)}

        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.rootViewController = container.resolve(LoginController.self)
        window?.makeKeyAndVisible()
        return true
    }
    

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

