//
//  DistributionProvider.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 14.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Alamofire
import Argo
import enum Result.NoError

class DistributionProvider{

    private let _loginDriverUrl = "http://api.kvasnica.back/distribution/distribution/login-driver"
    private let _distributionListUrl = "http://api.kvasnica.back/distribution/distribution/distribution-list"
    private let _distributionOrderListUrl = "http://api.kvasnica.back/distribution/distribution/distribution-order-list"
    private let _distributionOrderDetailUrl = "http://api.kvasnica.back/distribution/distribution/distribution-order"
    
    private var _authorizator : AuthorizatorProvider
    
    let driver = MutableProperty<Driver?>(nil)
    
    init(authorizator : AuthorizatorProvider){
        _authorizator = authorizator
    }
    
    func login(login : String, password : String){
        
        if driver.value == nil{
            driver <~ _authorizator
                .apiUser
                .producer
                .flatMap(.Latest){[weak self] apiUser -> SignalProducer<Driver, NoError> in
                    
                    if let user = apiUser{
                        return self!
                            ._loginDriver(login, password: password, token: user.token)
                            .flatMapError{_ in
                                return SignalProducer<Driver, NoError>.empty
                            }
                    }
                    
                    return SignalProducer<Driver, NoError>.empty
                }
                .map{$0}

        }

    }
    
    func getDistributionOrderDetail(distributionOrderId : Int) -> SignalProducer<DistributionOrderDetail, NoError>{
        return _authorizator
            .apiUser
            .producer
            .combineLatestWith(driver.producer)
            .flatMap(.Latest){[weak self] tuple -> SignalProducer<DistributionOrderDetail, NoError> in
                let apiUser = tuple.0
                let driver = tuple.1
                
                if let user = apiUser, d = driver{
                    return self!._getDistributionOrderDetail(d.id, distributionOrderId: distributionOrderId, token: user.token)
                        .flatMapError{_ in
                            return SignalProducer<DistributionOrderDetail, NoError>.empty
                        }
                }
                
                return SignalProducer<DistributionOrderDetail, NoError>.empty
                
            }
    }
    
    
    func getDistributionOrderList(distributionId : Int) -> SignalProducer<DistributionOrders, NoError>{
        return _authorizator
            .apiUser
            .producer
            .combineLatestWith(driver.producer)
            .flatMap(.Latest){[weak self] tuple -> SignalProducer<DistributionOrders, NoError> in
                
                let apiUser = tuple.0
                let driver = tuple.1
                
                if let user = apiUser, d = driver{
                  return self!._getDistributionOrderList(d.id, distributionId: distributionId, token: user.token)
                    .flatMapError{_ in
                        return SignalProducer<DistributionOrders, NoError>.empty
                    }
                }
                
                return SignalProducer<DistributionOrders, NoError>.empty
            }
    }
    
    
    func getDistributionList() -> SignalProducer<Distributions, NoError>{
        
        return driver
            .producer
            .ignoreNil()
            .combineLatestWith(_authorizator.apiUser.producer)
            .flatMap(.Latest){[weak self] tuple -> SignalProducer<Distributions, NoError> in
                let driver = tuple.0
                let apiUser = tuple.1
                
                if let user = apiUser{
                    return self!._getDistributionList(driver.id, token: user.token)
                        .flatMapError{_ in
                            SignalProducer<Distributions, NoError>.empty
                        }
                }
                
                return SignalProducer<Distributions, NoError>.empty
            }
    }
    
    
    private func _getDistributionOrderDetail(driverId: Int, distributionOrderId : Int, token : String) -> SignalProducer<DistributionOrderDetail, NSError>{
        let parameters = [
            "driver_id" : driverId,
            "id_distribution_order" : distributionOrderId
        ]
        
        let headers = [
            "ExsysAuthenticateToken" : token
        ]
        
        return Alamofire.Manager.rac_jsonRequest(.GET, _distributionOrderDetailUrl, parameters: parameters, headers: headers)
        
    }
    
    private func _getDistributionOrderList(driverId : Int, distributionId: Int, token : String) -> SignalProducer<DistributionOrders, NSError>{
        let parameters = [
            "driver_id" : driverId,
            "id_distribution" : distributionId
        ]
        
        let headers = [
            "ExsysAuthenticateToken" : token
        ]
        
        return Alamofire.Manager.rac_jsonRequest(.GET, _distributionOrderListUrl, parameters: parameters, headers: headers)
    }
    
    private func _getDistributionList(driverId : Int, token : String) -> SignalProducer<Distributions, NSError>{
        let parameters = [
            "driver_id" : driverId
        ]
        
        let headers = [
            "ExsysAuthenticateToken" : token
        ]
        
        return Alamofire.Manager.rac_jsonRequest(.GET, _distributionListUrl, parameters: parameters, headers: headers)
        
    }

    private func _loginDriver(login : String, password : String, token : String) -> SignalProducer<Driver, NSError>{
        
        let parameters = [
            "syslogin" : login,
            "syspassword" : password
        ]
        
        let headers = [
            "ExsysAuthenticateToken" : token
        ]
        
        
        return Alamofire.Manager.rac_jsonRequest(.POST, _loginDriverUrl, parameters: parameters, headers: headers)
    }
    
}