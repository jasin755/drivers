//
//  Authorizator.swift
//  Drivers
//
//  Created by Nikolaj Pognerebko on 14.06.16.
//  Copyright © 2016 Nikolaj Pognerebko. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Alamofire
import Argo
import enum Result.NoError

class AuthorizatorProvider {

    private let loginName = "tablet_rousinov_9"
    private let password = "NbiVEijp"
    private let url = "http://api.kvasnica.back/complaints/complaint/authentication"
    
    let apiUser = MutableProperty<ApiUser?>(nil)
    private let _apiUser = MutableProperty<ApiUser?>(nil)
        
    init(){
        apiUser <~ _apiUser
        .producer
            .flatMap(.Latest){[weak self] apiUser -> SignalProducer<ApiUser?, NoError> in
                if apiUser == nil{
                    return self!.loginApiUser()
                        .flatMapError{_ in
                            return SignalProducer<ApiUser, NoError>.empty
                        }
                        .flatMap(.Latest){user in
                            return SignalProducer<ApiUser?, NoError>(value: user)
                        }  
                }
                
                return SignalProducer<ApiUser?, NoError>.empty
            }
    }
    
    func loginApiUser() -> SignalProducer<ApiUser, NSError>{
        let parameters = [
            "login" : self.loginName,
            "password" : self.password
        ]
        
         return Alamofire.Manager.rac_jsonRequest(.POST, url, parameters: parameters)
    }
    
//    
//    //TODO: Pridat handlovani chyb
//    func getApiUser() -> MutableProperty<ApiUser?>{
//        
//        if apiUser.value == nil{
//            
//            let parameters = [
//                "login" : self.loginName,
//                "password" : self.password
//            ]
//            
//            let request : SignalProducer<ApiUser, NSError> = Alamofire.Manager.rac_jsonRequest(.POST, url, parameters: parameters)
//            
//            apiUser <~ request
//                .flatMapError{_ in
//                    SignalProducer<ApiUser, NoError>.empty
//                }
//                .map{$0}
//        }
//        
//        return apiUser
//
//    }
 
}
